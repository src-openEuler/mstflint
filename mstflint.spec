Name:           mstflint
Summary:        Firmware Burning and Diagnostics Tools
Version:        4.26.0
Release:        2
License:        GPLv2+ or BSD
Url:            https://github.com/Mellanox/mstflint
Source:         https://github.com/Mellanox/%{name}/releases/download/v%{version}-1/%{name}-%{version}-1.tar.gz
Patch0000:      0001-Fix-compile-errors.patch
Patch0001:      fix-return-local-addr.patch
BuildRequires:  libstdc++-devel zlib-devel rdma-core-devel gcc-c++ gcc
BuildRequires:  libcurl-devel libxml2-devel openssl-devel
Obsoletes:      openib-mstflint <= 1.4 openib-tvflash <= 0.9.2 tvflash <= 0.9.0

%description
This package contains a burning tool and diagnostic tools for Mellanox
manufactured HCA/NIC cards. It also provides access to the relevant source
code. Please see the file LICENSE for licensing details.

%package_help

%prep
%autosetup -p1

%build
export CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS"
%configure --enable-fw-mgr
%make_build

%install
%make_install
strip %{buildroot}/%{_libdir}/mstflint/python_tools/*.so
strip %{buildroot}/%{_libdir}/mstflint/sdk/*.so
%delete_la_and_a

%files
%doc README
%{_bindir}/*
%{_sysconfdir}/mstflint
%{_libdir}/mstflint
%{_datadir}/mstflint
%exclude %{_includedir}/

%files help
%{_mandir}/man1/*

%changelog
* Mon Apr 08 2024 liweigang <liweiganga@uniontech.com> - 4.26.0-2
- fix strip problem

* Wed Jan 24 2024 yaoxin <yao_xin001@hoperun.com> - 4.26.0-1
- Upgrade to 4.26.0

* Mon Oct 09 2023 wulei <wu_lei@hoperun.com> - 4.25.0-1
- Update to 4.25.0

* Wed Jul 19 2023 lvxiaoqian <xiaoqian@nj.iscas.ac.cn> - 4.10.0-12
- Add riscv patch from v4.17

* Tue Jul 04 2023 Ge Wang <wang__ge@126.com> - 4.10.0-11
- Fix build failure due to gcc updated

* Thu Jan 05 2023 chenmaodong <chenmaodong@xfusion.com> - 4.10.0-10
- Fix errors found with checkpatch script

* Thu Dec 29 2022 chenmaodong <chenmaodong@xfusion.com> - 4.10.0-9
- Fix error while burning mcc enabled Description

* Fri Sep 10 2021 Pengju Jiang <jiangpengju2@huawei.com> - 4.10.0-8
- solve the strip problem of dark conversion compilation

* Tue Aug 4 2021 shdluan@163.com <shdluan@163.com> - 4.10.0-7
- fix return local addr

* Tue Jul 28 2020 lingsheng <lingsheng@huawei.com> - 4.10.0-6
- change the libibmad-devel to rdma-core-devel

* Tue May 19 2020 lizhenhua <lizhenhua12@huawei.com> - 4.10.0-5
- Fix compile errors for gcc9

* Wed Dec 11 2019 catastrowings <jianghuhao1994@163.com> - 4.10.0-4
- openEuler init
